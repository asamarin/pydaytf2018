# Ticketeer - PyDay Tenerife 2018 Demo App

This is a toy MVC-like ticket management application written in Python 3, which I have dubbed **Ticketeer**. It uses [Sanic](https://sanic.readthedocs.io/en/latest/), [wtforms](https://wtforms.readthedocs.io/en/stable/), [jinja2](http://jinja.pocoo.org/docs/2.10/) and some other cool technologies that are probably unnecessary for such a simple app. It's meant to be deployed to [Google App Engine](https://cloud.google.com/appengine/) and uses [Firestore](https://cloud.google.com/firestore/) as the storage backend. The sole purpose of this little app is to show off GAE's scaling abilities during my talk at [PyDay Tenerife 2018](https://pythoncanarias.es/events/pydaytf18/).

### Deployment

If you want to deploy this to GAE yourself, you'll need to have an active Google Cloud account and working project with billing already set up. Also, you'll want to install [Google Cloud SDK](https://cloud.google.com/sdk/) as well. Follow the initial steps on e.g. the [Quickstart for Python App Engine Standard Environment](https://cloud.google.com/appengine/docs/standard/python/quickstart) if you're not sure how to do all this. Alternatively, if you'd rather not install any SDK locally you can deploy this app from [Cloud Shell](https://www.google.com/search?client=firefox-b-ab&q=google+cloud+shell), too.

Once all prerequisites are met, you're ready to deploy. First off, clone this repo:

```bash
git clone https://gitlab.com/asamarin/pydaytf2018.git
```

Get into the `app` directory:

```bash
cd pydaytf2018/app
```

Deploy:

```bash
gcloud app deploy
```

When this step completes, you can use `gcloud app browse` to open a new tab on your browser pointing at your `appspot.com` subdomain. Have fun!