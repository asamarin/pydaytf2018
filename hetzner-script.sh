#!/usr/bin/env bash

set -e

TOKEN=$(cat ~/.hetzner-token)
SERVER_LABEL="debian-hetzner"
SERVER_JSON_OBJECT=$(curl --silent \
                          --header "Authorization: Bearer $TOKEN" \
                          "https://api.hetzner.cloud/v1/servers")
SERVER_ID=$(jq --raw-output ".servers[] | select(.name == \"$SERVER_LABEL\") | .id" <<< "$SERVER_JSON_OBJECT")
SERVER_STATUS=$(jq --raw-output ".servers[] | select(.name == \"$SERVER_LABEL\") | .status" <<< "$SERVER_JSON_OBJECT")
SERVER_IPV4=$(jq --raw-output ".servers[] | select(.name == \"$SERVER_LABEL\") | .public_net.ipv4.ip" <<< "$SERVER_JSON_OBJECT")

if [[ $SERVER_STATUS == "off" ]]; then
    echo -n "\"$SERVER_LABEL\" is offline; booting..."
    # Turn server on
    STATUS_CODE=$(curl --silent \
                       --request POST \
                       --header "Content-Type: application/json" \
                       --header "Authorization: Bearer $TOKEN" \
                       "https://api.hetzner.cloud/v1/servers/$SERVER_ID/actions/poweron" \
                       --write-out "%{http_code}" \
                       --output /dev/null)
    if ! [[ $STATUS_CODE =~ ^20[0-4]$ ]]; then
        echo -e "\\nAPI call failed with status code $STATUS_CODE; aborting now!" >&2
        exit 1
    fi
    # Wait for it to boot, i.e. port 22 becomes reachable
    until ncat --wait 3 -z "$SERVER_IPV4" 22; do
        echo -n '.'
    done
fi

ssh "$USER"@"$SERVER_IPV4"
