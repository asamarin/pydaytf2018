#!/usr/bin/env bash

set -e

TOKEN=$(cat ~/.do-token)
DROPLET_LABEL="debian-do"
DROPLET_JSON_OBJECT=$(curl --silent \
                           --header "Authorization: Bearer $TOKEN" \
                           "https://api.digitalocean.com/v2/droplets?page=1&per_page=1")
DROPLET_ID=$(jq --raw-output ".droplets[] | select(.name == \"$DROPLET_LABEL\") | .id" <<< "$DROPLET_JSON_OBJECT")
DROPLET_STATUS=$(jq --raw-output ".droplets[] | select(.name == \"$DROPLET_LABEL\") | .status" <<< "$DROPLET_JSON_OBJECT")
DROPLET_IPV4=$(jq --raw-output ".droplets[] | select(.name == \"$DROPLET_LABEL\") | .networks.v4[0].ip_address" <<< "$DROPLET_JSON_OBJECT")

if [[ $DROPLET_STATUS == "off" ]]; then
    echo -n "\"$DROPLET_LABEL\" is offline; booting..."
    # Turn droplet on
    STATUS_CODE=$(curl --silent \
                       --request POST \
                       --header "Content-Type: application/json" \
                       --header "Authorization: Bearer $TOKEN" \
                       --data '{"type":"power_on"}' \
                       "https://api.digitalocean.com/v2/droplets/$DROPLET_ID/actions" \
                       --write-out "%{http_code}" \
                       --output /dev/null)
    if ! [[ $STATUS_CODE =~ ^20[0-4]$ ]]; then
        echo -e "\\nAPI call failed with status code $STATUS_CODE; aborting now!" >&2
        exit 1
    fi
    # Wait for it to boot, i.e. port 22 becomes reachable
    until ncat --wait 3 -z "$DROPLET_IPV4" 22; do
        echo -n '.'
    done
fi

ssh "$USER"@"$DROPLET_IPV4"
