#!/usr/bin/env bash

set -e

TOKEN=$(cat ~/.vultr-token)
SERVER_LABEL="debian-vultr"
SERVER_JSON_OBJECT=$(curl --silent \
                          --header "API-Key: $TOKEN" \
                          "https://api.vultr.com/v1/server/list")
SERVER_ID=$(jq --raw-output ".[] | select(.label == \"debian-vultr\") | .SUBID" <<< "$SERVER_JSON_OBJECT")
SERVER_STATUS=$(jq --raw-output ".[] | select(.label == \"debian-vultr\") | .power_status" <<< "$SERVER_JSON_OBJECT")
SERVER_IPV4=$(jq --raw-output ".[] | select(.label == \"debian-vultr\") | .main_ip" <<< "$SERVER_JSON_OBJECT")

if [[ $SERVER_STATUS == "stopped" ]]; then
    echo -n "\"$SERVER_LABEL\" is offline; booting..."
    # Turn server on
    STATUS_CODE=$(curl --silent \
                       --request POST \
                       --header "Content-type: application/x-www-form-urlencoded" \
                       --header "API-Key: $TOKEN" \
                       --data "SUBID=$SERVER_ID" \
                       "https://api.vultr.com/v1/server/start" \
                       --write-out "%{http_code}" \
                       --output /dev/null)
    if ! [[ $STATUS_CODE =~ ^20[0-4]$ ]]; then
        echo -e "\\nAPI call failed with status code $STATUS_CODE; aborting now!" >&2
        exit 1
    fi
    # Wait for it to boot, i.e. port 22 becomes reachable
    until ncat --wait 3 -z "$SERVER_IPV4" 22; do
        echo -n '.'
    done
fi

ssh "$USER"@"$SERVER_IPV4"
