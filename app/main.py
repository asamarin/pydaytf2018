import datetime
import uuid
from dataclasses import dataclass, asdict

from sanic import Sanic, response
from jinja2 import Environment, FileSystemLoader, Template
from wtforms import Form, StringField, SubmitField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, Length

from google.cloud import firestore

app = Sanic()
app.static('/static', './static')
template_env = Environment(
    loader=FileSystemLoader(searchpath='templates/'),
    enable_async=True
)
template = template_env.get_template('register.html')
db = firestore.Client()

@dataclass
class Ticket:
    '''Keeps track of tickets being sold'''
    tid: str
    first_name: str
    last_name: str
    email: str
    timestamp: datetime

class RegistrationForm(Form):
    first_name = StringField('First name',
                             validators=[DataRequired(), Length(min=2, max=30)],
                             render_kw={"placeholder": "John",
                                        "class": "form-control"})
    last_name = StringField('Last name',
                            validators=[DataRequired(), Length(min=2, max=30)],
                            render_kw={"placeholder": "Doe",
                                       "class": "form-control"})
    email = EmailField('Email Address',
                       validators=[DataRequired()],
                       render_kw={"placeholder": "john.doe@corp.com",
                                  "class": "form-control"})

@app.route('/', methods=['GET', 'POST'])
async def register(request):
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        random_uuid = str(uuid.uuid4())
        ticket = Ticket(tid=random_uuid,
                        first_name=form.first_name.data,
                        last_name=form.last_name.data,
                        email=form.email.data,
                        timestamp=datetime.datetime.now()
                 )
        db.collection(u'tickets') \
          .document(ticket.tid)   \
          .set(asdict(ticket))
        return response.redirect(app.url_for('register'))
    rendered_template = await template.render_async(form=form)
    return response.html(rendered_template)

if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine.
    app.run(host='127.0.0.1', port=8080)
