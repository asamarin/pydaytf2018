#!/usr/bin/env bash

set -e

TOKEN=$(cat ~/.linode-token)
LINODE_LABEL="debian-linode"
LINODE_JSON_OBJECT=$(curl --silent \
                          --header "Authorization: Bearer $TOKEN" \
                          "https://api.linode.com/v4/linode/instances")
LINODE_ID=$(jq --raw-output ".data[] | select(.label == \"$LINODE_LABEL\") | .id" <<< "$LINODE_JSON_OBJECT")
LINODE_STATUS=$(jq --raw-output ".data[] | select(.label == \"$LINODE_LABEL\") | .status" <<< "$LINODE_JSON_OBJECT")
LINODE_IPV4=$(jq --raw-output ".data[] | select(.label == \"$LINODE_LABEL\") | .ipv4[0]" <<< "$LINODE_JSON_OBJECT")

if [[ $LINODE_STATUS == "offline" ]]; then
    echo -n "\"$LINODE_LABEL\" is offline; booting..."
    # Turn linode on
    STATUS_CODE=$(curl --silent \
                       --request POST \
                       --header "Content-Type: application/json" \
                       --header "Authorization: Bearer $TOKEN" \
                       "https://api.linode.com/v4/linode/instances/$LINODE_ID/boot" \
                       --write-out "%{http_code}" \
                       --output /dev/null)
    if [[ $STATUS_CODE != 200 ]]; then
        echo -e "\\nAPI call failed with status code $STATUS_CODE; aborting now!" >&2
        exit 1
    fi
    # Wait for it to boot, i.e. port 22 becomes reachable
    until ncat --wait 3 -z "$LINODE_IPV4" 22; do
        echo -n '.'
    done
fi

ssh "$USER"@"$LINODE_IPV4"
